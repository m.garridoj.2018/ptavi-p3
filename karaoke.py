#!/usr/bin/python3
# -*- coding: utf-8 -*-


import urllib.request
import sys
import json
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler
from urllib.request import urlretrieve


class KaraokeLocal():
    def __init__(self, op1):
        try:
            parser = make_parser()
            cHandler = SmallSMILHandler()
            parser.setContentHandler(cHandler)
            self.lista = cHandler.get_tags()
            parser.parse(open(op1))
        except FileNotFoundError:
            sys.exit('Archivo no encontrado')

    def __str__(self):
        # Creamos un contenedor para las lineas, vacio
        line = ''
        #  Recorremos nuestra lista que contiene nuestros valores
        for valor in self.lista:
            line += valor[0]  # Nuestro elemento 1, por ejemplo root-layout
            for atributo in valor[1]:
                if valor[1][atributo] != "":
                    line += '\t' + atributo + '="' + valor[1][atributo] + '"'
        #  Valor de linea = tabulador horizontal atributo="" atributo2...
        #  Elemento será realmente los valores de nuestros atributos
            line += '\n'
        # Hemos puesto varias veces el line...
        #  print(valor[0] + '\t' + ...
        #  ...atributo + '="' + valor[1][atributo] + '"'.format(lista))
        #  print(line)
        return line

    def to_json(self, file_smil, fic_json=""):
        fic_json = file_smil.replace(".smil", ".json")

        with open(fic_json, "w") as jsonfile:
            json.dump(self.lista, jsonfile, indent=3)

    def do_local(self):
        for linea in self.lista:
            for valor in linea[1]:
                if valor == 'src':
                    if linea[1][valor].startswith('http'):
                        op1 = linea[1][valor].split('/')[-1]
                        urllib.request.urlretrieve(linea[1][valor], op1)
                        linea[1][valor] = op1
        #  Lo que hacemos en el for: buscamos el valor src
        #  Una vez encontrado si en la linea empieza por http
        #  entonces corta por detras cuando encuentre el primer /
        #  Lo pasa por la libreria request
        #  Nombramos op1 ahora con los cambios anteriores
        #    for valor, atributo in linea[1].items():
        #        if atributo[:7] == "http://":
        #            name_local = atributo.split('/')[-1]
        #            urlretrieve(atributo, name_local)
        #            print('Descargando %s....' % atributo)

        #  urllib.request.urlretrieve(linea[1].items[atributo],name_local[-1])


if __name__ == "__main__":
    try:
        op1 = sys.argv[1]
    except IndexError:
        sys.exit('Usage: python3 karaoke.py file.smil')
    file_karaoke = KaraokeLocal(op1)

    #  print(karaoke)
    print(file_karaoke)
    file_karaoke.to_json(op1)
    file_karaoke.do_local()
    file_karaoke.to_json(op1, 'local.json')
    print(file_karaoke)
