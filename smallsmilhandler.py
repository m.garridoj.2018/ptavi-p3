#!/usr/bin/python3
# -*- coding: utf-8 -*-
from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class SmallSMILHandler (ContentHandler):
    """
    Etiquetas SMIL que debe reconocer
    """
    def __init__(self):
        """
       Debemos crear una lista recogida por self para recoger los parametros
       """
        self.lista = []
        """
        Creamos un diccionario que recoga nuestras etiquetas y los atributos
        """
        self.attributos = {'root-layout': ['width', 'height', 'color'],
                           'region': ['id', 'top', 'bottom', 'left', 'right'],
                           'img': ['src', 'region', 'begin', 'end'],
                           'audio': ['src', 'begin', 'end'],
                           'textstream': ['src', 'region']}
        """
            Prueba con varios diccionarios. Dejamos un unico diccionario
        """
        #  self.root = {'root-layout': ['width', 'height', 'background-color']}
        #  self.region = {'region': ['id','top', 'bottom', 'left', 'right']}
        #  self.img = {'img' : ['src','region','begin','end']}
        #  self.audio = {'audio' : ['src','begin', 'end']}
        #  self.textstream = {'textstream' : ['src', 'region']}
    def startElement(self, name, attrs):
        diccionario = {}
        """
            En caso de diccionarios individuales
        """
        #  if name in self.root:
        #  for atributos in self.root[name]:
        #  diccionario[atributos] = attrs.get(atributos, '')
        #  Pasamos los valores de las etiquetas a la lista
        #  self.lista.append([name, diccionario])
        #  print(diccionario)
        #  crear un diccionario individual para cada uno
        #  if name == 'root-layout':
        #    self.root['Etiqueta'] = name
        #    self.width = attrs.get('width', "")
        #    self.root['atributo1'] = self.width
        #    self.height = attrs.get('height', "")
        #    self.root['atributo2'] = self.height
        #    self.background_color = attrs.get('background-color', "")
        #    self.root['atributo3'] = self.background_color
        #   self.lista.append(self.root)
        if name in self.attributos:
            for atributos in self.attributos[name]:
                diccionario[atributos] = attrs.get(atributos, '')
            self.lista.append((name, diccionario))
        #  Append sirve para añadir al final de nuestro diccionario.
        #  En este caso añade al final cada valor de nuestro diccionario
        #  items = diccionario.items()
        #  print(items)
        #  print(self.lista[:1])

    def get_tags(self):
        """
        Hacemos return a los valores recogidos en la lista creada
        """
        return self.lista


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    """
    Imprimimos por pantalla los valores de la lista
    """
    print(cHandler.get_tags())
